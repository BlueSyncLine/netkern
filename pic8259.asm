; 8259 Programmable Interrupt Controller routines

; Ports
PIC8259_PRI_CMD equ 0x20
PIC8259_SEC_CMD equ 0xa0

PIC8259_PRI_DATA equ 0x21
PIC8259_SEC_DATA equ 0xa1

; Commands
PIC8259_EOI equ 0x20


; Mask a given IRQ (in AL)
pic8259.mask:
    push ax
    push bx
    push cx
    push dx

    ; Store AL in CL
    mov cl, al

    ; Assume we're using the primary PIC
    mov dx, PIC8259_PRI_DATA

    ; But if our IRQ is bigger, switch
    test cl, 7
    jna .skip_secondary

    mov dx, PIC8259_SEC_DATA
    sub cl, 8
.skip_secondary:
    ; Unmask a given bit
    mov bl, 1
    shl bl, cl

    ; Apply the mask
    in al, dx
    or al, bl
    out dx, al

    ; Restore the registers
    pop dx
    pop cx
    pop bx
    pop ax
    ret


; Unmask a given IRQ (in AL)
pic8259.unmask:
    push ax
    push bx
    push cx
    push dx

    ; Store AL in CL
    mov cl, al

    ; Assume we're using the primary PIC
    mov dx, PIC8259_PRI_DATA

    ; But if our IRQ is bigger, switch
    test cl, 7
    jna .skip_secondary

    mov dx, PIC8259_SEC_DATA
    sub cl, 8
.skip_secondary:
    ; Unmask a given bit
    mov bl, 1
    shl bl, cl
    xor bl, 0xff

    ; Apply the mask
    in al, dx
    and al, bl
    out dx, al

    ; Restore the registers
    pop dx
    pop cx
    pop bx
    pop ax
    ret


; Signal EOI. AL is the IRQ number
pic8259.eoi:
    push ax
    push dx

    ; Do we only EOI the primary PIC?
    cmp al, 7

    ; Replace the contents of AL with our EOI command.
    mov al, PIC8259_EOI
    jna .skip_secondary

    mov dx, PIC8259_SEC_CMD
    out dx, al
.skip_secondary:
    mov dx, PIC8259_PRI_CMD
    out dx, al

    ; Restore the registers
    pop dx
    pop ax
    ret
