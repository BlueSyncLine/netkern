; El-Torito header and bootstrap code

bits 16
org 0

elTorito.start:
    ; Make sure we're in the proper segment
    jmp 0x7c0:elTorito.boot
    times 8-($-$$) db 0

; The Boot Information Table (filled in by genisoimage -boot-info-table)
elTorito.bootInfo:
    .primaryVolumeDescriptor dd 0
    .bootFileLocation        dd 0
    .bootFileLength          dd 0
    .checksum                dd 0
    .reserved                times 40 db 0

elTorito.boot:
    ; Initialize the segment registers
    cli
    mov ax, cs
    mov ds, ax

    ; The stack will be at 0x7000.
    mov ax, 0x7000
    mov ss, ax
    xor sp, sp
    sti

    jmp kernel.entryPoint
