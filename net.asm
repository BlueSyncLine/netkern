; Networking routines
NET_ARP_ETHERTYPE   equ 0x0608 ; 0x0806 (swapped)
NET_IPv4_ETHERTYPE  equ 0x0008 ; 0x0800 (swapped)

; Don't fragment
NET_IP_DF           equ 1<<1

; IP protocol numbers
NET_IP_PROTOCOL_TCP equ 0x06
NET_IP_PROTOCOL_UDP equ 0x11


; ARP padding
NET_ARP_PADDING     equ 64 - (net.ethernet._header.length + net.arp._header.length)

; DNS stuff
NET_DNS_QR          equ 1<<7
NET_DNS_QUERY       equ 0
NET_DNS_IQUERY      equ 1<<4
NET_DNS_STATUS      equ 2<<4
NET_DNS_AA          equ 1<<3
NET_DNS_TC          equ 1<<2
NET_DNS_RD          equ 1<<1
NET_DNS_RA          equ 1
NET_DNS_RCODE_MASK  equ 0x0f

; Network config (hardcoded)
net.config:
    .myIP       db 10, 0, 2, 123
    .gateway    db 10, 0, 2, 2
    .subnetMask db 255, 255, 255, 0

; Network context
net._context:
    .gatewayMAC times 6 db 0

; Ethernet header
net.ethernet._header:
    .dstMAC    times 6 db 0
    .srcMAC    times 6 db 0
    .etherType dw 0
    .length    equ $ - net.ethernet._header

; ARP header
net.arp._header:
    .htype  dw 0x0100               ; ARP over Ethernet
    .ptype  dw NET_IPv4_ETHERTYPE   ; IPv4 EtherType
    .hlen   db 6                    ; Ethernet MAC
    .plen   db 4                    ; IPv4 address
    .oper   dw 0x0100               ; Operation (request)
    .sha    times 6 db 0            ; Source MAC
    .spa    dd 0                    ; Source IPv4
    .tha    times 6 db 0            ; Target MAC
    .tpa    dd 0                    ; Target IPv4
    .length equ $ - net.arp._header

; IP header
net.ip._header:
    .verIHL      db 0x45            ; Version and IHL
    .fDSCPECN    db 0x00            ; DSCP/ECN are unused
    .totalLength dw 0               ; Total length
    .ident       dw 0               ; Identifier (unused)
    .fragOffset  dw 0               ; Fragment offset and flags
    .ttl         db 64              ; Time to Live
    .protocol    db 0               ; Protocol
    .checksum    dw 0               ; Header checksum
    .source      dd 0               ; Source address
    .destination dd 0               ; Destination address
    .length      equ $ - net.ip._header

; IP pseudo header
net.ip._pseudoHeader:
    .source      dd 0
    .destination dd 0
    .zeroes      db 0
    .protocol    db 0
    .totalLength dw 0
    .length      equ $ - net.ip._pseudoHeader

; UDP header
net.udp._header:
    .srcPort    dw 0                ; Source port
    .dstPort    dw 0                ; Destination port
    .dataLength dw 0                ; Data length
    .checksum   dw 0                ; Checksum
    .length     equ $ - net.udp._header


; Checksum routines

; Initializes the checksum in AX.
net._checksum.init:
    xor ax, ax
    ret

; Updates the checksum.
; DS:SI, CX define the data buffer.
net._checksum.update:
    push bx
.nextWord:
    ; Done?
    test cx, cx
    jz .done

    ; Load a word
    mov bx, [si]
    add si, 2

    ; Byte-swap
    xchg bh, bl

    ; Does it fall on a last byte?
    cmp cx, 1
    jnz .skipMask

    ; Mask the last byte
    xor bl, bl

    ; Increment CX
    inc cx
.skipMask:
    ; Add the word to the running sum
    add ax, bx

    ; Carry?
    jnc .skipCarry
.carry:
    add ax, 1

    ; Another carry
    jc .carry
.skipCarry:
    ; Continue
    sub cx, 2
    jmp .nextWord
.done:
    pop bx
    ret

; Finalizes the checksum.
net._checksum.final:
    ; Byte-swap the checksum
    xchg ah, al

    ; Invert it
    xor ax, 0xffff
    ret


; Computes a checksum for the IP header
net.ip._updateChecksum:
    push ax
    push cx
    push si

    ; Reset the checksum field
    mov word [net.ip._header.checksum], 0

    ; Compute the checksum
    call net._checksum.init

    mov si, net.ip._header
    mov cx, net.ip._header.length
    call net._checksum.update
    call net._checksum.final

    ; Store the checksum
    mov [net.ip._header.checksum], ax

    pop si
    pop cx
    pop ax
    ret

; Computes a checksum for the UDP header
; The payload is at DS:SI, CX.
net.udp._updateChecksum:
    push ax
    push cx
    push si

    ; Reset the checksum field
    mov word [net.udp._header.checksum], 0

    ; Compute the checksum
    call net._checksum.init

    ; Update the checksum with the data
    call net._checksum.update

    ; Update the checksum with the IP pseudo-header
    mov si, net.ip._pseudoHeader
    mov cx, net.ip._pseudoHeader.length
    call net._checksum.update

    ; Update the checksum with the UDP header
    mov si, net.udp._header
    mov cx, net.udp._header.length
    call net._checksum.update

    ; Finalize the checksum
    call net._checksum.final

    ; Store the checksum
    mov [net.udp._header.checksum], ax

    pop si
    pop cx
    pop ax
    ret


; Initialize the network stack
net.init:
    push si
    push di
    push es
    push eax
    push ecx

    ; ES = DS
    mov ax, ds
    mov es, ax

    ; Initialize the Ethernet controller
    call rtl8139.init

    ; Store our MAC in the Ethernet header
    mov si, rtl8139.mac
    mov di, net.ethernet._header.srcMAC
    mov cx, 6
    rep movsb

    ; Store our MAC in the ARP header
    mov si, rtl8139.mac
    mov di, net.arp._header.sha
    mov cx, 6
    rep movsb

    ; Resolve the gateway MAC
    mov si, net.gatewayMACMessage
    call console.print

    mov eax, [net.config.gateway]
    mov bx, net._context.gatewayMAC
    call net.arp.resolve

    ; Print the init message
    mov si, net.initMessage
    call console.print

    pop ecx
    pop eax
    pop es
    pop di
    pop si
    ret


; Resolve an IP using ARP
; EAX contains the IP we want to resolve
; EBX points to the MAC address
net.arp.resolve:
    push si
    push di
    push es
    push eax
    push ebx
    push ecx
    push edx

    ; Log
    mov si, net.arpRequestMessage
    call console.print
    call net.printIP
    call console.newline

    ; Store the target IP address
    mov [net.arp._header.tpa], eax

    ; Preserve it
    mov edx, eax

    ; ES = DS
    mov ax, ds
    mov es, ax

    ; Store our IP address too
    mov ecx, [net.config.myIP]
    mov [net.arp._header.spa], ecx

    ; Write the Ethernet packet

    ; Broadcast MAC
    mov di, net.ethernet._header.dstMAC
    mov cx, 3
    mov ax, 0xffff
    rep stosw

    ; Ethertype
    mov word [net.ethernet._header.etherType], NET_ARP_ETHERTYPE

    ; Prepare the registers
    mov ax, ETHERNET_TX_SEGMENT
    mov es, ax
    mov di, ETHERNET_TX_BUFFER

    ; Write the Ethernet header first
    mov si, net.ethernet._header
    mov cx, net.ethernet._header.length
    rep movsb

    ; And the ARP header
    mov si, net.arp._header
    mov cx, net.arp._header.length
    rep movsb

    ; And the padding
    mov al, 0x00
    mov cx, NET_ARP_PADDING
    rep stosb

    ; Length
    mov cx, 64

    ; Send the packet
    call rtl8139.sendPacket

.wait:
    ; Wait for a reply
    call rtl8139.wait

    ; Timeout?
    jc .timeout

    ; Read the received packet
    mov ax, ETHERNET_RX_SEGMENT
    mov es, ax

    mov si, ETHERNET_RX_BUFFER

    ; AX is the flags, CX is the length
    mov eax, [es:si]
    mov ecx, eax
    shr ecx, 16

    ; Not counting the checksum
    sub cx, 4

    ; Was the packet intended for us?
    test eax, RTL8139_PAM

    ; If not, wait again
    jz .wait

    ; ES:DI now points to the CX-byte-long received buffer
    add si, 4
    mov di, si

    ; If the length is not 64, it's not an ARP packet
    cmp cx, 64
    jnz .wait

    ; Is it an ARP packet (is htype right)?
    cmp word [es:di+14], 0x0100
    jnz .wait

    ; Is it an ARP reply?
    cmp word [es:di+14+6], 0x0200
    jnz .wait

    ; Is it for the address we're looking for?
    cmp [es:di+14+14], edx
    jnz .wait

    ; Make ES:DI point to the target MAC address
    add di, 22
    mov cx, 6

    ; Copy the MAC address

    ; Preserve BX
    mov dx, bx
.copyByte:
    mov al, [es:di]
    mov [bx], al
    inc di
    inc bx
    dec cx
    jnz .copyByte

    ; Log the ARP reply
    mov si, net.arpReplyMessage
    call console.print

    ; Print the MAC address
    mov bx, dx
    call net.printMAC
    call console.newline

.done:
    pop edx
    pop ecx
    pop ebx
    pop eax
    pop es
    pop di
    pop si
    ret

.timeout:
    ; Timeout message
    mov si, net.arpErrorMessage
    call console.print
    jmp .done

; Prints an IP address in EAX
net.printIP:
    push eax
    push edx
    push bx
    push cx

    mov edx, eax
    mov cl, 4
.printByte:
    ; Store the digit in AX, shift
    xor ax, ax
    xor bl, bl
    mov al, dl
    shr edx, 8

    ; Divide by 100, print digit
    mov ch, 100
    div ch

    ; Leading zero check
    add bl, al
    test bl, bl
    jz .skip100

    add al, 0x30
    call console.putchar

.skip100:
    mov al, ah
    xor ah, ah

    ; Divide by 10, print digit
    mov ch, 10
    div ch

    ; Leading zero check
    add bl, al
    test bl, bl
    jz .skip10

    add al, 0x30
    call console.putchar

.skip10:
    mov al, ah

    ; Ones
    add al, 0x30
    call console.putchar

    ; Next
    dec cl
    jz .done

    mov al, '.'
    call console.putchar
    jmp .printByte

.done:
    pop cx
    pop bx
    pop edx
    pop eax
    ret

; Prints a MAC address pointed to by BX
net.printMAC:
    push bx
    push cx
    push dx
    mov cx, 6

.printByte:
    ; Load
    mov dh, [bx]

    ; Print
    call console.printHexByte

    ; Next byte
    inc bx

    ; Done?
    dec cx
    jnz .printByte

    pop dx
    pop cx
    pop bx
    ret


; Resolves an IP address to a MAC address (set in the destination of the Ethernet frame header).
; EAX contains the target IP.
net.resolveIP:
    push eax
    push ebx
    push ecx
    push edx
    push si
    push di

    ; Log
    mov si, net.ipResolveMessage
    call console.print
    call net.printIP
    call console.newline

    ; Is the address that we want to resolve local, broadcast or remote?
    cmp eax, 0xffffffff
    jz .broadcast

    mov ebx, [net.config.myIP]
    and ebx, [net.config.subnetMask]
    mov edx, eax
    and edx, [net.config.subnetMask]
    cmp ebx, edx

    ; If yes, do an ARP resolve.
    jz .doARP

    ; Local
    mov si, net.ipRemoteMessage
    call console.print

    ; Write the gateway's MAC address
    mov si, net._context.gatewayMAC
    mov di, net.ethernet._header.dstMAC
    mov cx, 3

.storeWord:
    mov ax, [si]
    mov [di], ax
    inc bx
    inc dx
    dec cx
    jnz .storeWord


    jmp .done

    ; Do an ARP resolve otherwise
.doARP:
    mov si, net.ipLocalMessage
    call console.print

    mov bx, net.ethernet._header.dstMAC
    call net.arp.resolve
    jmp .done

.broadcast:
    mov si, net.ipBroadcastMessage
    call console.print

    mov ax, 0xff
    mov bx, net.ethernet._header.dstMAC
    mov cx, 3

.storeBroadcast:
    mov [bx], ax
    inc bx
    dec cx
    jnz .storeBroadcast

    ; Restore the registers, done
.done:
    pop di
    pop si
    pop edx
    pop ecx
    pop ebx
    pop eax
    ret


; UDP stuff

; Initialize the UDP protocol.
; EAX is the target IP, BX is the target port, DX is the source port.
net.udp.init:
    push eax
    push ebx
    push edx

    ; Store the ports and IP addresses as necessary.
    xchg bh, bl
    mov [net.udp._header.dstPort], bx

    xchg dh, dl
    mov [net.udp._header.srcPort], dx

    mov ebx, [net.config.myIP]

    ; IP header
    mov [net.ip._header.source], ebx
    mov [net.ip._header.destination], eax
    mov byte [net.ip._header.protocol], NET_IP_PROTOCOL_UDP

    ; IP pseudo-header
    mov [net.ip._pseudoHeader.source], ebx
    mov [net.ip._pseudoHeader.destination], eax
    mov byte [net.ip._pseudoHeader.protocol], NET_IP_PROTOCOL_UDP

    ; Ethernet header
    mov word [net.ethernet._header.etherType], NET_IPv4_ETHERTYPE

    ; Resolve the IP address
    call net.resolveIP

    pop edx
    pop ebx
    pop eax
    ret


; Send an UDP packet (at DS:SI), CX bytes long.
net.udp.sendPacket:
    push bx
    push dx
    push es
    push si
    push di
    push cx

    ; Preserve SI, CX
    mov bx, si
    mov dx, cx

    ; ES points to our buffer
    mov ax, ETHERNET_TX_SEGMENT
    mov es, ax

    ; Compute the total length (for IPv4)
    mov ax, net.ip._header.length
    add ax, net.udp._header.length
    add ax, cx
    xchg ah, al

    ; Write the total length to the header and the pseudo-header
    mov [net.ip._header.totalLength], ax

    ; Compute the data length (for UDP)
    mov ax, cx
    add ax, net.udp._header.length

    xchg ah, al
    mov [net.udp._header.dataLength], ax
    mov [net.ip._pseudoHeader.totalLength], ax

    ; Update the header checksum
    call net.ip._updateChecksum
    call net.udp._updateChecksum

    ; Copy the Ethernet header
    mov si, net.ethernet._header
    mov di, ETHERNET_TX_BUFFER
    mov cx, net.ethernet._header.length
    rep movsb

    ; Copy the IP header
    mov si, net.ip._header
    mov cx, net.ip._header.length
    rep movsb

    ; Copy the UDP header
    mov si, net.udp._header
    mov cx, net.udp._header.length
    rep movsb

    ; Copy the payload
    mov si, bx
    mov cx, dx
    rep movsb

    ; Do we need padding?
    mov cx, di
    sub cx, ETHERNET_TX_BUFFER
    cmp cx, 64
    jnb .skip_padding

    ; Simply include garbage data at the end (might leak data!)
    mov cx, 64

.skip_padding:
    call rtl8139.sendPacket

    pop cx
    pop di
    pop si
    pop es
    pop dx
    pop bx
    ret


; Receive an IP packet.
; ES:DI points to the received packet's payload, CX is the payload length.
; BL will contain the IP protocol number.
net.ip.recvPacket:
    push eax
    push edx
    push si

    mov si, net.ipWaitMessage
    call console.print

    ; Wait for an Ethernet packet
.wait:
    call rtl8139.wait
    jc .timeout

    ; Packet received, check it
    mov ax, ETHERNET_RX_SEGMENT
    mov es, ax
    mov di, ETHERNET_RX_BUFFER

    ; Get the length
    mov eax, [es:di]
    shr eax, 16
    mov cx, ax

    ; No checksum
    sub cx, 4

    ; Skip the header
    add di, 4

    ; Test the Ethertype
    mov ax, [es:di+12]
    cmp ax, NET_IPv4_ETHERTYPE

    ; Not an IPv4 packet, wait again
    jnz .wait

    ; Preserve the protocol number
    mov bl, [es:di+14+9]

    ; Test whether the IP matches ours.
    ; Is the address broadcast in the network?
    mov eax, [net.config.subnetMask]
    xor eax, 0xffffffff
    or eax, [net.config.myIP]

    cmp eax, [es:di+14+16]
    jz .match

    ; Is the address broadcast in 0.0.0.0?
    mov eax, 0xffffffff
    cmp eax, [es:di+14+16]
    jz .match

    ; Is the address our own?
    mov eax, [net.config.myIP]
    cmp eax, [es:di+14+16]
    jz .match

    ; No match, waiting again
    jmp .wait

.match:
    ; Skip the IP and Ethernet headers

    ; AL = IHL*4
    mov al, [es:di+14]
    and al, 0x0f
    shl al, 2
    xor ah, ah

    ; Subtract the header lengths
    sub cx, 14 ; Ethernet
    sub cx, ax ; IP

    ; Skip the headers
    add di, 14 ; Ethernet
    add di, ax ; IP

    mov si, net.ipWaitDoneMessage
    call console.print

.done:
    pop si
    pop edx
    pop eax
    ret

.timeout:
    mov si, net.ipTimeoutMessage
    call console.print

    stc
    jmp .done

net.initMessage db "[+] net: initialized the network stack", 13, 10, 0
net.arpRequestMessage db "[*] net: sending an ARP request, IP: ", 0
net.arpReplyMessage db "[*] net: got an ARP reply, the host is at: ", 0
net.gatewayMACMessage db "[*] net: obtaining the gateway MAC", 13, 10, 0
net.arpErrorMessage db "[!] net: ARP timeout!", 13, 10, 0

net.ipResolveMessage db "[*] net: resolving IP: ", 0
net.ipLocalMessage db "[*] net: IP is local", 13, 10, 0
net.ipRemoteMessage db "[*] net: IP is remote", 13, 10, 0
net.ipBroadcastMessage db "[*] net: IP is broadcast", 13, 10, 0
net.ipWaitMessage db "[*] net: waiting for an IP packet...", 13, 10, 0
net.ipWaitDoneMessage db "[*] net: done waiting!", 13, 10, 0
net.ipTimeoutMessage db "[*] net: IP timeout!", 13, 10, 0
