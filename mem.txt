Memory mapping:
    The kernel starts at 0x7c00.

    The Ethernet RX buffer should be 8192+1500+16 (9708) bytes long.
    The Ethernet TX buffer should be 1512 bytes long.
    The stack (64 KB) is at 0x70000 - 0x7ffff.

    Segments:
        CS 0x07c0
        DS 0x07c0
        SS 0x7000

    The RX buffer is located at 0x60000 - 0x625ec.
    The TX buffer is at 0x625ec - 0x62bd4.
        
